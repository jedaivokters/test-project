<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'todo'], function () {
    Route::get('list', 'TodoController@todos');
    Route::post('store', 'TodoController@store');
    Route::put('{id}/update', 'TodoController@doUpdate');
    Route::get('{id}/get', 'TodoController@detail');
    Route::delete('{id}/delete', 'TodoController@doDelete');
    Route::put('{id}/mark-complete', 'TodoController@markComplete');
});
