import Home from "../pages/home.vue";
import Details from "../pages/todo-detail.vue";
import Add from "../pages/todo-store.vue";
import Update from "../pages/todo-update.vue";

export default [
  { path: "", name: "todos", component: Home },
  { path: "/todo/:id/", name: "todo-details", component: Details },
  { path: "/todo/:id/update", name: "todo-update", component: Update },
  { path: "/todo/add", name: "todo-add", component: Add },
];
