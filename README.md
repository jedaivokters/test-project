# Set up

Backend built in PHP Laravel.
Frontend built in VueJS.

### Backend:

Note: Can use docker just go to `backend`folder then `docker-compose up -d`

---

1. Go to `backend/app` folder
2. `composer install`
3. Copy `.env.example` to `.env`
4. `php arisan migrate`
5. Serve in http://localhost

Note: For docker DB_HOST=db | DB_PASSWORD=secret

### Frontend:

1. Go to `frontend/app` folder
2. `npm install`
3. On another terminal `npm run serve` _(http://localhost:8080) default_

Note: `VUE_APP_API_URL=http://localhost` (need .env)

# TODOS:

1. Filtering
2. Sorting
3. Alter table for completion date
