import store from "./module/store";
import routes from "./module/routes";

export default {
  name: "todo_site",
  store: store,
  routes: routes,
};
