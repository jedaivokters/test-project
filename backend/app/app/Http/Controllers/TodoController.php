<?php

namespace App\Http\Controllers;

use App\Todo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TodoController extends Controller
{

	private $todo;
	private $response;
	private $statuses = [
		'todo' => 'todo',
		'completed' => 'completed'
	];

	/**
	 * TodoController constructor.
	 * 
	 * @param Todo $todo
	 */
	public function __construct(Todo $todo)
	{
		$this->todo = $todo;

		$this->response = [
			'message' => '',
			'data' => '',
			'errors' => null
		];
	}


	public function todos(Request $request)
	{
		$this->response['data'] = $this->todo->orderBy('id', 'desc')->get();
		return response()->json($this->response);
	}

	public function detail(Request $request, $id)
	{
		$this->response['data'] = $this->todo->find($id);
		return response()->json($this->response);
	}

	public function store(Request $request)
	{
		$request->validate([
			'title' => 'required|max:100',
			'note' => 'required'
		], [
			"name.required" => __('common.name_required'),
			'note.required' => __('common.note_required')
		]);

		$this->todo->insert([
			'title' => $request->title,
			'note' => $request->note,
			'status' => $this->statuses['todo'],
		]);

		return response()->json($this->response);
	}

	public function doUpdate(Request $request, $id)
	{
		$request->validate([
			'title' => 'required|max:100',
			'note' => 'required'
		], [
			"name.required" => __('common.name_required'),
			'note.required' => __('common.note_required')
		]);

		$this->todo->whereId($id)->update([
			'title' => $request->title,
			'note' => $request->note,
			'status' => $request->status,
		]);

		return response()->json($this->response);
	}

	public function doDelete(Request $request, $id)
	{
		$this->todo->find($id)->delete();

		return response()->json($this->response);
	}

	public function markComplete(Request $request, $id)
	{
		$this->todo->whereId($id)->update([
			'status' => $this->statuses['completed'],
		]);

		return response()->json($this->response);
	}
}
